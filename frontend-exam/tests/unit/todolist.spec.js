import { shallowMount } from '@vue/test-utils'
import Todolist from '@/components/Todolist.vue'

test('our component comes with a default value for status', () => {
  const wrapper = shallowMount(Todolist, {
    propsData: {
      todos: {id: 1, description: 'test', order: 0}
    }
  })
  expect(wrapper.props().id).toBe(1)
  expect(wrapper.props('description')).toBe('test')
  expect(wrapper.props().order).toBe(0)
})