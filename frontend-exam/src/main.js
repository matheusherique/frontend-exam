import Vue from 'vue';
import SuiVue from 'semantic-ui-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import VueResource from 'vue-resource';
import App from './App.vue';
import 'semantic-ui-css/semantic.min.css';

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674',
};

Vue.use(SuiVue);
Vue.use(VueResource);
Vue.use(VueSweetalert2, options);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
